package cn.qu.dockman.agent.scheduler.jobs;

import cn.qu.dockman.agent.scheduler.quartz.WsJob;
import cn.qu.dockman.protocol.command.client.PingCommand;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;


/**
 * Created by zh on 17/3/16.
 */
public class PingJob extends WsJob {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        getQueue().send(PingCommand.create());
    }


}
