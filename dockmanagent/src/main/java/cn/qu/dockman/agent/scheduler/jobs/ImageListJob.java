package cn.qu.dockman.agent.scheduler.jobs;

import cn.qu.dockman.agent.scheduler.quartz.WsJob;
import cn.qu.dockman.agent.util.DockmanClient;
import cn.qu.dockman.protocol.command.client.ImageListCommand;
import com.github.dockerjava.api.model.Image;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zh on 17/3/16.
 */
public class ImageListJob extends WsJob {

    @Autowired
    private DockmanClient dockmanClient;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<Image> imageList = dockmanClient.listImagesCmd().exec();

        List<ImageListCommand.ImageListBody> bodyList = new ArrayList<>();
        for (Image image : imageList) {
            ImageListCommand.ImageListBody body = new ImageListCommand.ImageListBody();
            BeanUtils.copyProperties(image, body);
            bodyList.add(body);
        }

        ImageListCommand command = ImageListCommand.create(bodyList.toArray(new ImageListCommand.ImageListBody[0]));
        getQueue().send(command);

    }

}
