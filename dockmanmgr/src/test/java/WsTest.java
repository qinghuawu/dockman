import java.net.URISyntaxException;

/**
 * Created by zh on 17/3/8.
 */
public class WsTest {
    public static void main(String[] args) throws URISyntaxException {
//
//        WebSocketClient wsClient = new WebSocketClient(new URI("ws://localhost:8080/websocket")) {
//            @Override
//            public void onOpen(ServerHandshake serverHandshake) {
//                System.out.println(serverHandshake.toString());
//            }
//
//            @Override
//            public void onMessage(String s) {
//                System.out.println(s);
//            }
//
//            @Override
//            public void onClose(int i, String s, boolean b) {
//                System.out.println("close");
//            }
//
//            @Override
//            public void onError(Exception e) {
//                e.printStackTrace();
//            }
//        };
//        wsClient.connect();
//
        int i = 219;

        byte[] bs = int2byte4(219);
        for (byte b : bs) {
            System.out.println(b);
        }

        System.out.println(byte42int(bs));

    }

    protected static int byte42int(byte[] bytes) {
        int n = 0;
        for (int i = bytes.length - 1; i >= 0; i--) {
            n <<= 4;
            n += bytes[i];
        }
        return n;
    }

    protected static byte[] int2byte4(int length) {
        byte[] bytes = new byte[4];
        int len = length;
        for (int i = 0; i < 4; i++) {
            bytes[i] = (byte) (len & 0xF);
            len >>= 4;
        }
        return bytes;
    }
}
