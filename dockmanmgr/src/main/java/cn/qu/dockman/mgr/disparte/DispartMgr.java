package cn.qu.dockman.mgr.disparte;

import java.util.Observable;

/**
 * Created by zh on 17/3/16.
 */
public class DispartMgr extends Observable {
    @Override
    public void notifyObservers(Object arg) {
        setChanged();
        super.notifyObservers(arg);
    }
}
