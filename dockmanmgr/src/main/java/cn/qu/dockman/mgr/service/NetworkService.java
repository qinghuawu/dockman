package cn.qu.dockman.mgr.service;

import cn.qu.dockman.mgr.entity.Network;

import java.util.List;

/**
 * Created by zh on 17/3/10.
 */
public interface NetworkService {

    Network findNetwork(int hostId, String networkId);

    void saveOrUpdate(Network network);

    List<Network> findByHostId(int hostId);

    void delete(Network network);
}
