package cn.qu.dockman.mgr.disparte;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.entity.Metrics;
import cn.qu.dockman.mgr.service.HostService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by zh on 17/3/16.
 */
public class HostObserver implements Observer {

    public HostObserver(DispartMgr dispartMgr) {
        dispartMgr.addObserver(this);
    }

    @Autowired
    private HostService hostService;

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof ArgDef) {
            ArgDef def = (ArgDef) arg;
            switch (def.getName()) {
                case "CPU_TOTAL": {
                    Host host = hostService.findByIp(def.getHost());
                    if (host != null) {
                        Metrics metrics = (Metrics) def.getData();
                        host.setHostCpu(Integer.valueOf(metrics.getMetricsValue()));
                        hostService.updateHost(host);
                    }
                }
                break;
                case "MEM_TOTAL": {
                    Host host = hostService.findByIp(def.getHost());
                    if (host != null) {
                        Metrics metrics = (Metrics) def.getData();
                        host.setHostMem(Integer.valueOf(metrics.getMetricsValue()));
                        hostService.updateHost(host);
                    }
                }
                break;
                case "CPU_USED": {
                    Host host = hostService.findByIp(def.getHost());
                    if (host != null) {
                        Metrics metrics = (Metrics) def.getData();
                        host.setHostCpuUsed(Integer.valueOf(metrics.getMetricsValue()));
                        hostService.updateHost(host);
                    }
                }
                break;
                case "MEM_USED": {
                    Host host = hostService.findByIp(def.getHost());
                    if (host != null) {
                        Metrics metrics = (Metrics) def.getData();
                        host.setHostMemUsed(Integer.valueOf(metrics.getMetricsValue()));
                        hostService.updateHost(host);
                    }
                }
                break;
            }
        }
    }
}
