package cn.qu.dockman.mgr.entity;

import java.util.Date;

public class Host {
    private Integer id;

    private String hostName;

    private String hostIp;

    private Integer hostCpu;

    private Integer hostCpuUsed;

    private Integer hostMem;

    private Integer hostMemUsed;

    private Integer hostDisk;

    private Integer hostDiskUsed;

    private Integer hostStatus;

    private Integer envId;

    private Date createTime;

    private Date updateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHostName() {
        return hostName;
    }

    public void setHostName(String hostName) {
        this.hostName = hostName == null ? null : hostName.trim();
    }

    public String getHostIp() {
        return hostIp;
    }

    public void setHostIp(String hostIp) {
        this.hostIp = hostIp == null ? null : hostIp.trim();
    }

    public Integer getHostCpu() {
        return hostCpu;
    }

    public void setHostCpu(Integer hostCpu) {
        this.hostCpu = hostCpu;
    }

    public Integer getHostCpuUsed() {
        return hostCpuUsed;
    }

    public void setHostCpuUsed(Integer hostCpuUsed) {
        this.hostCpuUsed = hostCpuUsed;
    }

    public Integer getHostMem() {
        return hostMem;
    }

    public void setHostMem(Integer hostMem) {
        this.hostMem = hostMem;
    }

    public Integer getHostMemUsed() {
        return hostMemUsed;
    }

    public void setHostMemUsed(Integer hostMemUsed) {
        this.hostMemUsed = hostMemUsed;
    }

    public Integer getHostDisk() {
        return hostDisk;
    }

    public void setHostDisk(Integer hostDisk) {
        this.hostDisk = hostDisk;
    }

    public Integer getHostDiskUsed() {
        return hostDiskUsed;
    }

    public void setHostDiskUsed(Integer hostDiskUsed) {
        this.hostDiskUsed = hostDiskUsed;
    }

    public Integer getHostStatus() {
        return hostStatus;
    }

    public void setHostStatus(Integer hostStatus) {
        this.hostStatus = hostStatus;
    }

    public Integer getEnvId() {
        return envId;
    }

    public void setEnvId(Integer envId) {
        this.envId = envId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}