package cn.qu.dockman.mgr.disparte;

import cn.qu.dockman.mgr.entity.Host;
import cn.qu.dockman.mgr.service.HostService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by zh on 17/3/16.
 */
public class PingObserver implements Observer {

    @Autowired
    private HostService hostService;

    public PingObserver(DispartMgr dispartMgr) {
        dispartMgr.addObserver(this);
    }

    @Override
    public void update(Observable o, Object arg) {

        if (arg instanceof ArgDef) {
            ArgDef def = (ArgDef) arg;
            switch (def.getName()) {
                case "ping": {
                    Host host = hostService.findByIp(def.getHost());
                    if (host != null) {
                        host.setHostStatus(1);
                        hostService.updateHost(host);
                    }
                }
                break;
            }
        }
    }
}