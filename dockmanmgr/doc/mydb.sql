/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 100121
Source Host           : localhost:3306
Source Database       : mydb

Target Server Type    : MYSQL
Target Server Version : 100121
File Encoding         : 65001

Date: 2017-03-07 12:02:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for docker_container
-- ----------------------------
DROP TABLE IF EXISTS `docker_container`;
CREATE TABLE `docker_container` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `container_name` varchar(100) NOT NULL COMMENT '容器名称',
  `container_status` int(11) NOT NULL COMMENT '状态 1=正常 。。。',
  `host_id` int(11) NOT NULL COMMENT '主机id',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='容器表';

-- ----------------------------
-- Records of docker_container
-- ----------------------------

-- ----------------------------
-- Table structure for docker_env
-- ----------------------------
DROP TABLE IF EXISTS `docker_env`;
CREATE TABLE `docker_env` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `env_name` varchar(32) NOT NULL COMMENT '环境名称',
  `env_code` varchar(32) NOT NULL COMMENT '环境编码',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `env_code` (`env_code`) USING HASH
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='环境表';

-- ----------------------------
-- Records of docker_env
-- ----------------------------

-- ----------------------------
-- Table structure for docker_host
-- ----------------------------
DROP TABLE IF EXISTS `docker_host`;
CREATE TABLE `docker_host` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_name` varchar(100) NOT NULL COMMENT '主机名称',
  `host_ip` varchar(100) NOT NULL,
  `host_cpu` int(11) NOT NULL COMMENT 'CPU总量',
  `host_mem` int(11) NOT NULL COMMENT '内存总量',
  `host_disk` int(11) NOT NULL COMMENT '磁盘空间总量',
  `env_id` int(11) NOT NULL COMMENT '所属环境',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='主机表';

-- ----------------------------
-- Records of docker_host
-- ----------------------------

-- ----------------------------
-- Table structure for docker_image
-- ----------------------------
DROP TABLE IF EXISTS `docker_image`;
CREATE TABLE `docker_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_name` varchar(100) NOT NULL COMMENT '镜像名称',
  `image_text` text COMMENT '镜像简介',
  `image_url` varchar(255) NOT NULL COMMENT '镜像地址',
  `image_latest` varchar(32) NOT NULL COMMENT '镜像最近版本号',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='镜像表';

-- ----------------------------
-- Records of docker_image
-- ----------------------------

-- ----------------------------
-- Table structure for docker_tag
-- ----------------------------
DROP TABLE IF EXISTS `docker_tag`;
CREATE TABLE `docker_tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(30) NOT NULL COMMENT '标签名称',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='标签表';

-- ----------------------------
-- Records of docker_tag
-- ----------------------------

-- ----------------------------
-- Table structure for docker_version
-- ----------------------------
DROP TABLE IF EXISTS `docker_version`;
CREATE TABLE `docker_version` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `version_name` varchar(30) NOT NULL COMMENT '版本号',
  `image_id` int(11) NOT NULL COMMENT '镜像ID',
  `version_text` text COMMENT '版本描述',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of docker_version
-- ----------------------------

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', '张三', '23');
INSERT INTO `t_user` VALUES ('2', '张三', '23');
INSERT INTO `t_user` VALUES ('3', '11', '11');
INSERT INTO `t_user` VALUES ('4', '22', '22');
INSERT INTO `t_user` VALUES ('5', '33', '33');
INSERT INTO `t_user` VALUES ('6', '44', '44');
INSERT INTO `t_user` VALUES ('7', '张三', '23');
INSERT INTO `t_user` VALUES ('8', '张三', '23');
