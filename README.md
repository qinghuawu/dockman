#dockman

docker 管理工具java版

采用Agent+Server的部署方式基于websocket协议交互运行，docker宿主机安装Agent后，可在Server断自动发现，并获取docker基本信息，伪实时（定时）页面刷新。
已初步完成基本功能，暂未做优化。

为什么不用更成熟的nio或aio？答案是想验证一下websocket在服务端间的的稳定性，后续有可能会更改协议为nio或aio。

项目总体开发思路以由简入繁为主，先最基本实现功能，再逐步优化重构，目前处理demo开发阶段。

待基本功能相对完善后，下一步将引入规则引擎处理指标数据。

项目采用springboot+mybatis+mysql开发，前端angularjs+bootstrap+echarts。
由于是基于ws协议部署无状态请求，可通过nginx或haproxy配置反向代理和Server水平部署方式，实现Server系统的高可用。

主要组件版本：
1. springboot-1.5.2.RELEASE
1. mybatis-spring-boot-starter-1.2.0
1. Java-WebSocket-1.3.0
1. sigar-1.6.4
1. docker-java-3.0.6
1. MariaDB-10.1.18

1. angularjs-1.6.2
1. bootstrap-3.3.7
1. angular-ui-2.2.0
1. echarts-3.2.2
1. jquery-1.12.1

由于是基于ws协议部署无状态请求，可通过nginx或haproxy配置反向代理和Server水平部署方式，实现Server系统的高可用。

项目背景：
目前已做了一年多的devops系统，对资源、自动化部署有相对丰富的经验。闲暇时非常喜欢docker容器化技术，所以打算融合docker+持续集成+自动化部署开发一套适合中小型公司的可持续交付系统。

欢迎对此项目提出宝贵建议，谢谢！

