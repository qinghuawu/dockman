package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;

/**
 * Created by zh on 17/3/15.
 */
public class ImageListCommand extends Command<ImageListCommand.ImageListBody> {

    public static final String NAME = "imageList";

    public static ImageListCommand create(ImageListBody[] body) {
        return new ImageListCommand(new Header(NAME), body);
    }

    private ImageListCommand(Header header, ImageListCommand.ImageListBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class ImageListBody implements Body {
        private Long created;

        private String id;

        private String parentId;

        private String[] repoTags;

        private Long size;

        private Long virtualSize;

        public Long getCreated() {
            return created;
        }

        public void setCreated(Long created) {
            this.created = created;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getParentId() {
            return parentId;
        }

        public void setParentId(String parentId) {
            this.parentId = parentId;
        }

        public String[] getRepoTags() {
            return repoTags;
        }

        public void setRepoTags(String[] repoTags) {
            this.repoTags = repoTags;
        }

        public Long getSize() {
            return size;
        }

        public void setSize(Long size) {
            this.size = size;
        }

        public Long getVirtualSize() {
            return virtualSize;
        }

        public void setVirtualSize(Long virtualSize) {
            this.virtualSize = virtualSize;
        }
    }
}