package cn.qu.dockman.protocol.command.client;

import cn.qu.dockman.protocol.command.Command;
import cn.qu.dockman.protocol.def.Body;
import cn.qu.dockman.protocol.def.Header;
import com.google.gson.Gson;

/**
 * Created by zh on 17/3/8.
 */
public class PingCommand extends Command<PingCommand.PingBody> {

    public static final String NAME = "ping";

    public static PingCommand create() {
        return new PingCommand(new Header(NAME), new PingBody[]{new PingBody()});
    }

    private PingCommand(Header header, PingBody[] body) {
        super(header, body);
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static class PingBody implements Body {

    }
}
